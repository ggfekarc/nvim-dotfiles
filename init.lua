require("configs.keybinds") --Keybindings
require("configs.plugins") --packer
require("configs.base") --General Settings
require("configs.lsp_config") -- LSP Configs
require("configs.completions") -- Auto completion
require("configs.rust_config") -- Rusty
